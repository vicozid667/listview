package bo.com.relativity.listviewa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etNombre, etPassword;
    private Button btnIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarVistas();
    }

    private void iniciarVistas() {
        etNombre = findViewById(R.id.etNombre);
        etPassword = findViewById(R.id.etPassword);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pasarALista();
            }
        });
    }

    private void pasarALista() {
        String nombre, password;
        nombre = etNombre.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        if(nombre.equalsIgnoreCase("sergio") && password.equalsIgnoreCase("123456")) {
            Intent intent = new Intent(this, ListaActivity.class);
            intent.putExtra("nombre", nombre);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Sus credenciales son incorrectas", Toast.LENGTH_LONG).show();
        }
    }
}
