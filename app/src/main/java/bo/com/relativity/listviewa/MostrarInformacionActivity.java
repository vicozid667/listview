package bo.com.relativity.listviewa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MostrarInformacionActivity extends AppCompatActivity {

    TextView tvLibro, tvInformacion;

    List<String> detalles = new ArrayList<>();

    String libro;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_informacion);
        inicializarVistas();
    }

    private void inicializarVistas() {
        tvLibro = findViewById(R.id.tvLibro);
        tvInformacion = findViewById(R.id.tvInformacion);
        detalles.add("Loren ipsum dolorem bla bla bla");
        detalles.add("Loren ipsum dolorem bla bla bla");
        detalles.add("Loren ipsum dolorem bla bla bla");
        detalles.add("Loren ipsum dolorem bla bla bla");
        detalles.add("Loren ipsum dolorem bla bla bla");
        detalles.add("Loren ipsum dolorem bla bla bla");
        detalles.add("Loren ipsum dolorem bla bla bla");
        detalles.add("Loren ipsum dolorem bla bla bla");
        recibirDatos();
    }

    private void recibirDatos() {
        Bundle extras = getIntent().getExtras();
        libro = extras.getString("libro");
        pos = extras.getInt("pos");
        tvLibro.setText(libro);
        tvInformacion.setText(detalles.get(pos));

    }

}
