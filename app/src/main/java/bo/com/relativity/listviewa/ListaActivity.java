package bo.com.relativity.listviewa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListaActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView lista;

    private List<String> listaLibros;

    private String nombreUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        inicializarVistas();
    }

    private void inicializarVistas(){
        // 1. conectar o inicializar o asignar nuestro atributo lista (ListView)
        //al component visual a través de su ID
        lista = findViewById(R.id.listView);

        //2. cargar la lista que tenemos con elementos.
        listaLibros = new ArrayList<>();
        listaLibros.add("El horror en Dunwich");
        listaLibros.add("La llamada de Cthutlu");
        listaLibros.add("1984");
        listaLibros.add("La granja de cerdos");
        listaLibros.add("Agujeros negros y espacio curvo");
        listaLibros.add("La iliada");
        listaLibros.add("La odisea");
        listaLibros.add("Juego de Ender");
        listaLibros.add("Una canción de hielo y fuego");
        listaLibros.add("danza de dragones");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add("El Hobbit");
        listaLibros.add(".........");

        // 3. Adaptador -> lo que hace es transformar la lista en este caso de
        // Strings al formato visualizable de Vista en la ListView....
        // Agarra cada elemento y los pinta como elementos visuales en la ListView


        //Para este ejercicio vamos a usar un Adaptador que ya existe en Android
        // este adaptador ya existente es como una plantilla predefinida
        // tecnicamente que existe un fichero xml que define como va a ser
        //dibujado cada elemento de la lista
        // Usaremos el llamado ArrayAdapter

        ArrayAdapter adaptador = new ArrayAdapter(
                this, // primer argumento es referencia al contexto del activity en q nos encontramos
                android.R.layout.simple_list_item_1, // segundo argumento hace referencia al template que vamos a usar para diseñar los elementos de la vista
                listaLibros // tercer argumento la lista de elementos que queremos cargar sobre la listView
        );


        //4. Vinculación del ListView -> Adapter
        lista.setAdapter(adaptador);

        //5. gestionar el evento click del ListView
        lista.setOnItemClickListener(this);



        //para recuperar la data de la actividad 1
        recuperarValoresActividad1();
    }

    private void recuperarValoresActividad1() {
        Bundle extras = getIntent().getExtras();
        nombreUsuario = extras.getString("nombre");

        //Mostrar un dato String en la cabecera del aplicativo
        setTitle(nombreUsuario);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String libro = listaLibros.get(position);
        Toast.makeText(this,"El Libro es: " + libro, Toast.LENGTH_LONG).show();
        Intent i = new Intent(this, MostrarInformacionActivity.class);
        i.putExtra("libro", libro);
        i.putExtra("pos", position);
        startActivity(i);
    }
}
